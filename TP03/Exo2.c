#include <stdio.h>
#include <stdlib.h>

void initialiser_tableau(char* tab,int taille);
void affiche_tableau (char * tableau, int taille);

typedef struct {
char * tableau;
int taille;
} super_tableau_t;


int main(void){
    super_tableau_t* super_tab = malloc(sizeof(super_tableau_t));
    super_tab->taille = 4;
    super_tab->tableau = malloc(super_tab->taille * sizeof(char));
    initialiser_tableau(super_tab->tableau,super_tab->taille);
    affiche_tableau (super_tab->tableau,super_tab->taille);
}

void initialiser_tableau(char* tab,int taille){
    tab[0] = 'a';
    for(int i = 0; i < taille; i++){
        tab[i] = tab[0] + i;
    }
}

void affiche_tableau (char * tableau, int taille){
    printf("\n");
    for(int i = 0; i < taille; i++){
        printf(" %c",tableau[i]);
    }
    printf("\n\n");
}