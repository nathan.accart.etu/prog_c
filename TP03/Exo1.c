#include <stdio.h>
#include <stdlib.h>

char * nouveau_tableau (int taille);
void initialise_tableau (char * tableau, int taille, char car);
void affiche_tableau (char * tableau, int taille);
void liberation_du_tableau (char * tableau);
void place_dans_tableau (char * tableau, int taille, int indice, char car);
char lecture_du_tableau (char * tableau, int taille, int indice);

int main(void){
    char* tab = nouveau_tableau (4);
    initialise_tableau (tab,4,'a');
    affiche_tableau (tab,4);
    //liberation_du_tableau (tab);
    //affiche_tableau (tab,4);
    place_dans_tableau (tab,4,2,'z');
    affiche_tableau (tab,4);
    lecture_du_tableau (tab, 4, 21);
}

char * nouveau_tableau (int taille){
    char* pt;
    pt = malloc(taille*sizeof(char));
    return pt;
}

void initialise_tableau (char * tableau, int taille, char car){
    for(int i = 0; i < taille; i++){
        tableau[i] = 'a'+ i;
    }
    tableau[2] = 'e';
}

void affiche_tableau (char * tableau, int taille){
    printf("\n");
    for(int i = 0; i < taille; i++){
        printf(" %c",tableau[i]);
    }
    printf("\n\n");
}

void liberation_du_tableau (char * tableau){
    free(tableau);
    printf("Le tablo conti1%c", *tableau);
}

void place_dans_tableau (char * tableau, int taille, int indice, char car){
    tableau[indice] = car;
    if(indice > taille || indice < taille){
        printf("Erreur...Erreur...Erreur");
    }

}

char lecture_du_tableau (char * tableau, int taille, int indice){
    char car;
    if(indice > taille || indice < 0){
        tableau[indice] = '\0';
        printf("Erreur...Erreur...Erreur");
        car = '\0';
    }else{
        car = tableau[indice];
        printf("Char : %c", car);
    }
    return car;
}