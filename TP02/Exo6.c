#include <stdio.h>

void alphabet (char * tab, int taille);
char valeur_tableau_2d (char * tableau, int nb_ligne,int nb_colonne,int pos_ligne, int pos_colonne);
void affiche_tableau_2d (char * tableau, int nb_ligne, int nb_colonne);
void place_valeur_tableau_2d (char * tableau, int nb_ligne, int nb_colonne, int pos_ligne, int pos_colonne, char car);

int main(void){
    char tab[16];
    printf("Question 1)\n");
    alphabet (tab, 16);
    printf("%c\n\n",valeur_tableau_2d (tab,4,4,0,0));
    printf("Question 2)\n");
    affiche_tableau_2d(tab,4,4);
    place_valeur_tableau_2d (tab,4,4,0,0,'Z');
    printf("Question 3)\n");
    affiche_tableau_2d(tab,4,4);
}

void alphabet (char * tab, int taille){

    for (int i = 0; i<taille;i++){
        tab[i] = 'a' + i;
    }

}

char valeur_tableau_2d (char * tab, int nb_ligne,int nb_colonne,int pos_ligne, int pos_colonne){

    return tab[pos_ligne * nb_colonne + pos_colonne];
}

void affiche_tableau_2d (char * tableau, int nb_ligne, int nb_colonne){

    for(int i = 0; i < nb_ligne;i++){
        for(int j = 0; j < nb_colonne; j++){
            printf("%c  ",valeur_tableau_2d (tableau,nb_ligne,nb_colonne,i,j));
        }
        printf("\n\n");
    }
}

void place_valeur_tableau_2d (char * tableau, int nb_ligne, int nb_colonne, int pos_ligne, int pos_colonne, char car){
    tableau[pos_ligne * nb_colonne + pos_colonne] = car;
}

