#include <stdio.h>

void demande_valeur(char * entree, int taille);
void affiche_tableau (char * tab, int taille);

int main(void){
    char tab[10];
    demande_valeur(tab, 10);
    affiche_tableau(tab,20);
    printf("\n\n");
}

void demande_valeur(char * entree, int taille){
    printf("Saisie qqc :");
    scanf("%s",entree);
}   

void affiche_tableau (char * tab, int taille){
    for(int i = 0; i < taille; i++){
        printf("%c",tab[i]);
    }
}