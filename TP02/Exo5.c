#include <stdio.h>

void affiche_caractere (char car);
void alphabet (char * tableau, int taille);
void alphabet_renverse (char * tableau, int taille);
void affiche_tableau (char * tableau, int taille);

int main(void){
    affiche_caractere ('a');

    char tab[6];
    alphabet (tab, 6);
    alphabet_renverse (tab,6);

    affiche_tableau (tab, 6);
}

void affiche_caractere (char car){
    printf("\n Question 1)\n%c --> ",car);
    printf("%d\n\n",car);
}

void alphabet (char * tab, int taille){
    printf("\n Question 2)\n");
    for (int i = 0; i<taille;i++){
        tab[i] = 'a' + i;
        printf("%c ",tab[i]);
    }
    printf("\n\n");

}

void alphabet_renverse (char * tab, int taille){
    printf("\n Question 3)\n");
    for (int i = 0; i<taille;i++){
        tab[i] = 'z' - i;
        printf("%c ",tab[i]);
    }
    printf("\n\n");
}

void affiche_tableau (char * tab, int taille){
    printf("\n Question 4)\n");
    for(int i = 0; i < taille; i++){
        printf("%c ",tab[i]);
    }
    printf("\n\n");
    tab[3] = '\0';
    for(int i = 0; i < taille; i++){
        printf("%c ",tab[i]);
    }
    printf("\nIl enleve le caractère \n\n");

    printf("\n Question 5)\n");
    tab[2] = '\0';
    tab[4] = '\0';
    for(int i = 0; i < taille; i++){
        printf("%c ",tab[i]);
    }
    printf("\n\n");
}

