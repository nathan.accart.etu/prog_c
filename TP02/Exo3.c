#include <stdio.h>

void somme_et_produit (int a, int b, int * pt_somme, int * pt_produit);

int main (void){
    int a = 4; 
    int b = 7;
    int* pt_somme; 
    int* pt_produit;
    pt_somme = &a;
    pt_produit = &b;
    printf("\n\n Question 1)\nValeur de la case pointée par pt_sommme avant <somme_et_produit> : %d\n",* pt_somme);
    printf("Valeur de la case pointée par pt_produit avant <somme_et_produit> : %d",* pt_produit);
    somme_et_produit (4, 7, pt_somme, pt_produit);
    printf("\n\nValeur de la case pointée par pt_sommme après <somme_et_produit> : %d\n",* pt_somme);
    printf("Valeur de la case pointée par pt_produit après <somme_et_produit> : %d\n\n",* pt_produit);
}

void somme_et_produit (int a, int b, int * pt_somme, int * pt_produit){
    *pt_somme = a + b;
    *pt_produit = a * b;
}