#include <stdio.h>

void test_pointeur (void);
int plus_egal (int * pt_a, int b);
int fois_egal (int * pt_a, int b);

int main (void){
    
    test_pointeur();

    int addition = 10;
    int* pt_addition;
    pt_addition = &addition;
    plus_egal (pt_addition, 3);
    printf("Dans le main :%d\n\n",addition);
    fois_egal (pt_addition, 3);
    printf("Dans le main :%d\n\n",addition);
}

void test_pointeur (void){
    int a = 10;
    int* pt_a;
    pt_a = &a;

    printf("\nQuestion 1)\nadresse de entier      : %p\n", &a);
    printf("pointeur vaut          : %p\n\n", pt_a);
}

int plus_egal (int *pt_addition, int b){
    int addition = *pt_addition + b;
    *pt_addition = addition;
    printf("Question 2)\nRésultat de l'addition  = %d\n",addition);
    return(0);
}

int fois_egal (int *pt_addition, int b){
    int addition = *pt_addition * b;
    *pt_addition = addition;
    printf("Question 3)\nRésultat de la multiplication = %d\n",addition);
    return(0);
}