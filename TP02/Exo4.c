#include <stdio.h>

void affiche_tableau (char * tableau, int taille);
void affiche_tableau_pointeur (char * tableau, int taille);
void change_lettre (char * pt_lettre, char nouvelle_lettre, int idx);

int main(void){
    char tableau[6] = {'a','z','e','r','t','y'};
    printf("\nQuestion 1)\n");
    affiche_tableau (tableau, 6);

    printf("\n\nQuestion 2)\n");
    affiche_tableau_pointeur (tableau, 6);

    printf("\n\nQuestion 3)\n");
    char * pt_lettre = &tableau[0];
    change_lettre (pt_lettre, 'u', 2);
    affiche_tableau (tableau, 6);
    printf("\n\n");
}

void affiche_tableau (char * tableau, int taille){
    for(int i = 0; i < taille; i++){
        printf("%c ",tableau[i]);
    }
}

void affiche_tableau_pointeur (char * tableau, int taille){

    char * ptr = &tableau[0];
    for(int i = 0; i < taille; i++){
        printf("%c ",*(ptr + i));
    }

}

void change_lettre (char * pt_lettre, char nouvelle_lettre, int idx){
    *(pt_lettre+idx) = nouvelle_lettre;
}