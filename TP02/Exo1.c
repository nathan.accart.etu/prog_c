#include <stdio.h>

void affiche_entier (int n);
int produit (int a, int b);
void modifie_a (int a);

int main (void){
    affiche_entier(10);

    produit(10,5);

    int val = 13;
    modifie_a(val);
    printf("Nouvelle valeure de a : %d\n\n",val);
}
void affiche_entier (int n){
    int pred = n - 1;
    printf("\nQuestion 1)\nLa fonction vous affiche l'entier : %d",pred);
}

int produit (int a, int b){
    int res = a * b;
    printf("\n\nQuestion 2) \nLe résultat est : %d", res);
    return(0);
}

void modifie_a (int a){
    a = a + 1;
    printf("\n\nQuestion 3) \nNouvelle valeure de a : %d\n",a);
}