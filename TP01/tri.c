#include <stdio.h>

int main(void){

    int tab[6] = {5,4,7,14,2,3};
    for(int i = 0; i < 6; i++){
        printf("tab[%d] = %d \n", i,tab[i]);
    }

    int tmp = 0;
    
    for(int i = 0; i < 5; i++){
        if(tab[i] < tab[i+1]){
            tmp = tab[i];
            tab[i] = tab[i+1];
            tab[i+1] = tmp;
        }
    }
    for(int i = 0; i < 6; i++){
        printf("\ntab[%d] = %d", i,tab[i]);
    }
}