#include <stdio.h>

int main(void){
    int tab[6] = {5,4,7,1,2,3};
    int grande_valeure;
    int position;
    for(int i = 0; i < 6; i++){
        if(tab[i]> grande_valeure){
            grande_valeure = tab[i];
            position = i;
        }
    }
    printf("La valeur maximale est %d\n", grande_valeure);
    printf("Elle se trouve en position %d\n\n", position);
}