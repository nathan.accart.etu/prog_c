#include <stdio.h>

int main(void){
    float devise = 60;
    float taux_change = 1.09;

    float x = devise * taux_change;

    printf("Alice et Bob ont %f euros après conversion\n", x);
    printf("Ils gardent donc %f euros chacun\n\n", x/2);
}